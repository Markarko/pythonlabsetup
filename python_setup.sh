#!/bin/bash

# Check if the required number of parameters are passed
if [ "$#" -ne 2 ]; then
  echo "Error: Please provide 2 parameters: <repository name> <gitlab username>"
  exit 1
fi

# Store the passed parameters in variables
repo_name=$1
username=$2
path=$(pwd)

# Check if setup.bat exists
if ! [ -f "./setup.bat" ]; then
	echo "Looks like you are missing the setup.bat script"
	exit 1
fi

# Check if the gitignore exists
if ! [ -f "./.gitignore" ]; then
	echo "Looks like you are missing the gitignore file. Please download it from the link below and put it inside the current directory"
	echo "https://moodle.dawsoncollege.qc.ca/pluginfile.php/1425800/mod_resource/content/2/.gitignore"
	exit 1
fi

# Check if there is already a directory with the repository's name
if [ -d "./$repo_name" ]; then
	echo "Looks like a directory with the name '$repo_name' already exists"
	echo "Please delete it to proceed"
	exit 1
fi

# Check if curl is installed
if ! command -v curl > /dev/null; then
  echo "Error: curl is not installed. Please install it to run this script."
  exit 1
elif ! command -v python > /dev/null; then
  echo "Error: python is not installed. Please install it to run this script."
  exit 1
fi

# Show or don't show what the script does
echo "Would you like to see the things that the script does for you? (y/n)"
read choice
if [[ $choice == "y" ]]; then
	echo "1) Creates a private gitlab repo with the name you specified"
	echo "2) Adds adds the gitignore that you downloaded from moodle"
	echo "3) Adds the teacher to the repo as a maintainer"
	echo "4) Creates the virtual environment"
	echo "5) Adds the requirements.txt file"
fi

# Get the private token
echo "You will be prompted your gitlab token"
echo "Would you like to see the instructions on how to create the token? (y/n)"

# Show or don't show instructions to get private token
read choice
if [[ $choice == "y" ]]; then
	echo "To get your token, click on your account icon on the top right and click edit profile"
	echo "From the left menu, click 'access tokens' and create your token"
	echo "Make sure to select 'api' and 'read_api' scopes when creating the token"
fi
echo "Enter your GitLab private token: "
read -s private_token

# Create the repository
create_repo_response=$(curl --request POST --header "Private-Token: $private_token" "https://gitlab.com/api/v4/projects?name=$repo_name&visibility=private")

# Check if the repository was created successfully
if echo "$create_repo_response" | grep "created" > /dev/null; then
  echo "Repository '$repo_name' created successfully."
else
  echo "Error: Failed to create repository. create_repo_response: $create_repo_response"
  exit 1
fi

# Add the gitignore file
git clone "https://gitlab.com/$username/$repo_name"
cd $repo_name
cp ../.gitignore ./
git checkout -b develop
git add *
git commit -m "Added gitignore"
git push origin develop

# Add empty readme
touch README.md
git add *
git commit -m "Added empty readme"
git push origin develop

# Adding the teacher to our repository
repository_id=$(curl --header "Private-Token: $private_token" "https://gitlab.com/api/v4/projects?&search=$repo_name" | grep -Po '\{"id":[^}]+\}' | grep -Po '"id":\s*\K[^,]+' | head -n 1)
invite_member_response=$(curl --header "Private-Token: $private_token" -X POST --data "user_id=9740400&access_level=40" "https://gitlab.com/api/v4/projects/$repository_id/invitations")

# Run the rest in command prompt
echo $path/setup.bat
start $path/setup.bat $path $repo_name


