This script autoconfigures the server-side development lab setup  
  
Things to do before running the script:  
1) Download the gitignore file from the following link  
https://moodle.dawsoncollege.qc.ca/pluginfile.php/1425800/mod_resource/content/2/.gitignore  
and place it in the same directory where you will run the script  
2) Get your GitLab private token. Run the script to find out how to get it  
  
Here's what the script does:  
1) Creates a private gitlab repo with the name you specified  
2) Adds the gitignore that you downloaded from moodle  
3) Adds the teacher to the repo as a maintainer  
4) Creates the virtual environment  
5) Adds the requirements.txt file  

NOTE: after cloning the repository, move the 2 scripts outside the folder and run them like shown below  
(you can delete the cloned repository after moving them). If you simply downloded the 2 scripts, you can ignore this.  
  
To run the script:  
1) Download python_setup.sh and setup.bat  
2) Run python_setup.sh in GitBash like you see below:  
./python_setup.sh (repository name) (gitlab username)    
  
Once you see a command prompt open, follow the instructions on the screen
 
