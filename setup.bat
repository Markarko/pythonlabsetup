@ECHO OFF

set argCount=0
for %%x in (%*) do (
	set /A argCount+=1
)

if %argCount% EQU 2 (
	cd %1\%2
	python -m venv .venv
	.venv\scripts\activate
	echo "Type 'start ..\setup.bat y' and press enter"
) else (
	pip install flask
	pip freeze > requirements.txt

	git status
	git add *
	git commit -m "Added requirements.txt"
	git push origin develop

	code .
)

